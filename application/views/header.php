<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <!-- importacion para dispocitivos moviles responsibilidad  -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Matrículas</title>
    <!-- IMPORTACION JQUERY -->
    <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
      <!-- FIN DE IMPORTACION  -->
      <br>
      <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    </head>
  <body>
    <div class="text-center">
      <img src="<?php echo base_url(); ?>/assets/img/logo.png" alt="Logo" height="100px">
    </div>

        <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo site_url(); ?>">APP MATRICULAS</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Estudiantes <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo site_url('estudiantes/index'); ?>">LISTADO </a></li>
          <li><a href="<?php echo site_url('estudiantes/nuevo'); ?>">NUEVO </a></li>

                    </ul>
            </li>

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Asignaturas <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo site_url('asignaturas/index'); ?>">LISTADO </a></li>
          <li><a href="<?php echo site_url('asignaturas/nuevo'); ?>">NUEVO </a></li>

                    </ul>
            </li>

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tipos De Matriculas <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo site_url('tipos/index'); ?>">LISTADO </a></li>
          <li><a href="<?php echo site_url('tipos/nuevo'); ?>">NUEVO </a></li>

                    </ul>
            </li>

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">colegio JMA<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo site_url('colegios/index'); ?>">LISTADO </a></li>
          <li><a href="<?php echo site_url('colegios/nuevo'); ?>">NUEVO </a></li>

                    </ul>
            </li>
          </ul>
          <form class="navbar-form navbar-left">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Search">
            </div>
            <button type="submit" class="btn btn-default">BUSQUEDA</button>
          </form>
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> DOCENTES<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo site_url('docentes/index'); ?>">LISTADO </a></li>
          <li><a href="<?php echo site_url('docentes/nuevo'); ?>">NUEVO </a></li>
                    </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">CURSOS  <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo site_url('cursos/index'); ?>">LISTADO </a></li>
                <li><a href="<?php echo site_url('cursos/nuevo'); ?>">NUEVO </a></li>
                  </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
